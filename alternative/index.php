<!doctype html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
<title>Ruddernation Designs&trade;</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="author" content="Ruddernation Designs"/>
<meta name="Copyright" content="Ruddernation Designs 1998-2016"/>
<link rel="shortcut icon" href="https://www.ruddernation.com/info/images/ico/favicon.ico">
<link rel="stylesheet" href="https://www.ruddernation.com/info/backgrounds/background.css"/>
<link rel="stylesheet" href="css/style.css">
</head>
<body>
  <form method="post" class="form">
  <input type="text" name="room" title="Enter Room Name, If it does not exist then it will create the room for you." placeholder="Just enter the name of the Tinychat room or create your own" id="roomname" list="roomdata" autofocus required/>
<input type="submit" class="button2" value="Chat"/></form>
<?php
$room = filter_input(INPUT_POST, 'room');
if(preg_match('/^[a-z0-9]/', $room=strtolower($room), $room=strip_tags($room)))
{
$room=preg_replace('/[^a-zA-Z0-9]/','',$room);
if (strlen($room) < 3)
$room = substr($room, 0, 0);
if (strlen($room) > 36)
$room = substr($room, 0, 36);
$prohash = hash('sha256',filter_input(INPUT_SERVER, 'HTTP_CLIENT_IP'));
}
if(empty($room))
{
	echo '<p>Enter a Tinychat room name to join that chat room.</p>';
return;
}
$chat = array(
		'room' 			=> $room,
		'prohash'		=> $prohash,
		'chatSmileys		=> 'true',
		'wmode'			=> 'transparent',
		'youtube'		=> 'all',
		'urlsuper'		=> 'http://www.ruddernation.com/chat',
		'desktop'		=> 'true', 
		'langdefault'		=>  'en' ); 
$chat['room'] = $room;
	foreach ( $chat as $r => $u ) { $chatString .= "{$r}: '{$u}', "; }
	$chat = substr( $chatString, 0); ?>
<div id="chat">
<script type=text/javascript>var embed;
tinychat = ({<?php echo $chat; ?>})</script>
<script src="https://www.ruddernation.com/info/js/eslag.js"></script>
<div id="client"> </div></div>
</body>
</html>
