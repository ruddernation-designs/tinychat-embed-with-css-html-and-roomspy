<!doctype html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
<title>Ruddernation Designs</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="Ruddernation Designs"/>
<meta name="keywords" content="ruddernation,monachechat,monache,tinychat,tinychat roomspy,tinychat clone,HTML5,CSS3,RUDD3R,wpe pro,tinychat hacks,peeps chat,cutedane"><meta name="author" content="Ruddernation Designs"/>
<link rel="shortcut icon" href="https://www.ruddernation.com/info/images/ico/favicon.ico">
<link rel="stylesheet" href="css/normalize.css">
<link rel="stylesheet" href="css/chat.css">
<script src="js/jquery/jquery.js"></script>
</head>
<body>
<div class="notification-text">
<div class="form">
<form method="post" class="form">
Room name:<input type="text" name="room" title="Enter Room Name, If it does not exist then it will create the room for you." placeholder="ruddernation"/>
<input type="submit" class="button" value="Enter"/>
<a href="/roomspy" class="button">Spy</a> <a href="/alternative" class="button">alternative</a></form></div></div>
<?php 
$room = filter_input(INPUT_POST, 'room');
if(preg_match('/^[a-z0-9]/', $room=strtolower($room)))
{
$room=preg_replace('/[^a-zA-Z0-9]/','',$room);
if (strlen($room) < 3)
$room = substr($room, 0, 0);
if (strlen($room) > 36)
$room = substr($room, 0, 36);
$room=strip_tags($room);
$prohash = hash('sha256',filter_input(INPUT_SERVER, 'HTTP_CLIENT_IP'));
echo '<div id="chat">
<script type="text/javascript">
var tinychat = ({room: "'.$room.'", 
		prohash: "'.$prohash.'",
		wmode:"transparent",
		chatSmileys:"true", 
		youtube:"all",
		urlsuper: "http://www.ruddernation.com", 
		desktop:"true",
		langdefault:"en"});
		</script>
<script src="https://www.ruddernation.com/info/js/eslag.js"></script>
<div id="client"></div></div>';
	}?>
</body>
</html>
