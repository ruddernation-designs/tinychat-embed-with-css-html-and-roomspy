# TinyChat instant chat and room spy
<p>This is the basic version of chat with the css and html for a quick and simple chat site,
This also contains a room spy which also includes css so you can upload to your own websites and use them instantly.</p>

<p>I've now added an alternative to this chat in the alternative folder, This does the same job but is in an array and easier to modify 
for your own use.</p>
<br/>
###<a href="http://ruddernation-designs.github.io/tinychat" target="_blank" title="Chat Demo">Chat Demo</a>
###<a href="https://www.tinychat-spy.com" target="_blank" title="TinyChat Room Spy Demo">Room Spy</a>
